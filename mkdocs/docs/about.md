# About

### Linkedin: [Harel Ben Attia](https://www.linkedin.com/in/harelba/)

### Twitter [@harelba](https://twitter.com/harelba)

### Email [harelba@gmail.com](mailto:harelba@gmail.com)

### Patreon [harelba](https://www.patreon.com/harelba)
All the money received is donated to the [Center for the Prevention and Treatment of Domestic Violence](https://www.gov.il/he/departments/bureaus/molsa-almab-ramla) in my hometown - Ramla, Israel.

### Chinese translation [jinzhencheng@outlook.com](mailto:jinzhencheng@outlook.com)

