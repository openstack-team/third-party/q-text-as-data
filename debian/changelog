q-text-as-data (3.1.6-5) UNRELEASED; urgency=medium

  * Set the fix-deprecated-datetime-utcnow.patch forwarded URL.

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Jul 2024 03:17:54 +0200

q-text-as-data (3.1.6-4) unstable; urgency=medium

  * Add fix-datetime-deprecated.patch (Closes: #1074707).
  * Bump debhelper-compat to 11.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Fri, 12 Jul 2024 03:35:11 +0200

q-text-as-data (3.1.6-3) unstable; urgency=medium

  * Aslo clean .pytest_cache (Closes: #1045174).

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Aug 2023 11:44:56 +0200

q-text-as-data (3.1.6-2) unstable; urgency=medium

  * Py 3.11 patch: remove-U-mode-when-opening-files.patch (Closes: #1028139).

 -- Thomas Goirand <zigo@debian.org>  Mon, 09 Jan 2023 09:02:04 +0100

q-text-as-data (3.1.6-1) unstable; urgency=medium

  * New upstream release (Closes: #953876).
  * Fixed running tests.
  * Fixed installing q in /usr/bin.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Dec 2021 11:14:49 +0100

q-text-as-data (1.7.4+2018.12.21+git+28f776ed46-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Removed Python 2 support (Closes: #938320).

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Sep 2019 17:05:38 +0200

q-text-as-data (1.7.4+2018.12.21+git+28f776ed46-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * Running wrap-and-sort -bast
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release (Closes: #888688):
    - Fixes Python 3 compat (Closes: #920537).
    - Add python{3,}-six as (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Jan 2019 00:10:07 +0100

q-text-as-data (1.4.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Correcting permissions in debian packaging files.
  * Updating standards version to 4.0.1.

  [ Thomas Goirand ]
  * Using openstack-pkg-tools for packaging.
  * Fixed build-depends versions.
  * Fixed gbp.conf.
  * Added dh-python as build-depends.
  * Now doing: dh_python3 --shebang=/usr/bin/python3 (Closes: #867463)
  * Fixed debian/copyright ordering.

 -- Thomas Goirand <zigo@debian.org>  Thu, 17 Aug 2017 12:59:55 +0200

q-text-as-data (1.4.0-1) unstable; urgency=low

  * Initial release. (Closes: #771777)

 -- Thomas Goirand <zigo@debian.org>  Tue, 02 Dec 2014 17:27:02 +0800
